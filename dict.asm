%define OFFSET 8

section .text

extern string_equals

global find_word

find_word:
    push rax
    push r8
    push r9
    push rdi
    push rsi
    add rsi, OFFSET
    call string_equals
    pop rsi
    pop rdi
    pop r9
    pop r8
    pop rax
    cmp rax, 1
    je .found
    mov rsi, [rsi]
    test rsi, rsi
    jnz find_word
    xor rax, rax
    ret
    .found:
        mov rax, rsi
        ret
