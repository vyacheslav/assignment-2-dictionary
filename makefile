ASM=nasm
ASMFLAGS=-f elf64

dictionary: main.o dict.o lib.o
	LD -o $@ $^

main.asm: lib.inc words.inc colon.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean
clean:
	RM *.o main