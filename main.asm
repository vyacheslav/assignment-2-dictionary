%include "lib.inc"
%include "words.inc"
extern find_word
global _start

section .data

find_error:
db "Word not found!", 0
read_error:
db "Word length exceeded!", 0

section .text

_start:
	mov rsi, 256
	mov rdi, rsp
	sub rdi, rsi
	call read_line
	test rax, rax
	jz read_error_label
	mov rdi, rax
	push rdx
	mov rsi, next
	call find_word
	test rax, rax
	jz find_error_label
	pop rdx
	add rax, 8
	add rax, rdx
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit

find_error_label:
        mov rdi, find_error
        call print_string
		xor rdi, rdi
        call exit

read_error_label:
        mov rdi, read_error
		call print_string
		xor rdi, rdi
        call exit
