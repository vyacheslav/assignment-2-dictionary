%define next 0

%macro colon 2

        %%this:

        dq next

        db %1, 0

        %define next %%this

        %2:

%endmacro
