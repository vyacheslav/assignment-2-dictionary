%define EXIT 60

%define END_LINE 0xA

%define SPACE 0x20

%define TAB 0x9

%define DECIMAL_BASE 10



section .text



global exit

global string_length

global print_string

global print_char

global print_newline

global print_uint

global print_int

global string_equals

global read_char

global read_word

global parse_uint

global parse_int

global string_copy

global read_line



; Принимает код возврата и завершает текущий процесс

exit: 

    mov rax, EXIT

    syscall

    ret



; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:

    xor rax, rax

    .loop:

        cmp byte[rdi+rax], 0

        je .end

        inc rax

        jmp .loop

    .end:

        ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout

print_string:

    push rdi

    call string_length

    pop rdi

    mov rdx, rax

    mov rsi, rdi

    mov rax, 1

    mov rdi, 1

    syscall

    ret



; Принимает код символа и выводит его в stdout

print_char:

    xor rax, rax

    push rdi

    mov rsi, rsp

    mov rdx, 1

    mov rax, 1

    mov rdi, 1

    syscall

    pop rdi

    ret



; Переводит строку (выводит символ с кодом 0xA)

print_newline:

    mov rdi, END_LINE

    call print_char

    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 

; Совет: выделите место в стеке и храните там результаты деления

; Не забудьте перевести цифры в их ASCII коды.

print_uint:

    mov rbx, DECIMAL_BASE

    mov rax, rdi

    xor rcx, rcx

    .write:

        xor rdx, rdx

        div rbx

        push rdx

        inc rcx

        cmp rax, 0

        jnz .write

    .print:

        pop rdx

        add rdx, '0'

        mov rdi, rdx

        push rcx

        push rdx

        push rdi

        push rax

        call print_char

        pop rax

        pop rdi

        pop rdx

        pop rcx

        dec rcx

        cmp rcx, 0

        jnz .print

    ret



; Выводит знаковое 8-байтовое число в десятичном формате 

print_int:

    or rdi, rdi

    jns .end

    push rdi

    mov rdi, '-'

    call print_char

    pop rdi

    neg rdi

    .end:

        jmp print_uint

        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:

    xor rax, rax

    xor r8, r8

    xor r9, r9

    .loop:

        cmp byte[rdi+rax], 0

        jz .check

        cmp byte[rsi+rax], 0

        jz .false

        mov r8b, [rdi+rax]

        mov r9b, [rsi+rax]

        cmp r8b, r9b

        jnz .false

        inc rax

        jmp .loop



    .check:

        cmp byte[rsi+rax], 0

        jz .true

    .false:

        xor rax, rax

        ret

    .true:

        mov rax, 1

        ret





; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

read_char:

    xor rax, rax

    xor rdi, rdi

    mov rdx, 1

    push 0

    mov rsi, rsp

    syscall

    pop rax

    ret 



; Принимает: адрес начала буфера, размер буфера

; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .

; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.

; Останавливается и возвращает 0 если слово слишком большое для буфера

; При успехе возвращает адрес буфера в rax, длину слова в rdx.

; При неудаче возвращает 0 в rax

; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    xor rdx, rdx

    .skip:

        push rdi

        push rsi

        push rdx

        call read_char

        pop rdx

        pop rsi

        pop rdi

        cmp rax, SPACE

        jz .skip

        cmp rax, TAB

        jz .skip

        cmp rax, END_LINE

        jz .skip

    .read:

        cmp rax, 0

        jz .goodend

        cmp rax, SPACE

        jz .goodend

	cmp rax, TAB

        jz .goodend

	cmp rax, END_LINE

        jz .goodend

        cmp rsi, rdx

        je .badend

        mov byte[rdi+rdx], al

        inc rdx

        push rdi

        push rsi

        push rdx

        call read_char

        pop rdx

        pop rsi

        pop rdi

        jmp .read

    .goodend:

        mov byte[rdi+rdx], 0

        mov rax, rdi

        ret

    .badend:

        xor rax, rax

        xor rdx, rdx

        ret



; Принимает указатель на строку, пытается

; прочитать из её начала беззнаковое число.

; Возвращает в rax: число, rdx : его длину в символах

; rdx = 0 если число прочитать не удалось

parse_uint:

    xor rax, rax

    xor rcx, rcx

    mov rbx, DECIMAL_BASE

    .read:

        xor r8, r8

        mov r8b, byte [rdi + rcx]

        sub r8, '0'

        cmp r8, 0

        jl .end

        cmp r8, 9

        jg .end

        mul rbx

        add al, r8b

        inc rcx

        jmp .read

    .error:

        xor rdx, rdx

        ret

    .end:

        xor 	rbx, rbx

        cmp 	rcx, 0

        je 	.error

        mov rdx, rcx

        ret





; Принимает указатель на строку, пытается

; прочитать из её начала знаковое число.

; Если есть знак, пробелы между ним и числом не разрешены.

; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 

; rdx = 0 если число прочитать не удалось

parse_int:

    cmp byte[rdi], '-'

    jne parse_uint

    inc rdi

    call parse_uint

    neg rax

    inc rdx

    ret





; Принимает указатель на строку, указатель на буфер и длину буфера

; Копирует строку в буфер

; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:

    push rdx

    push rdi

    push rsi

    call string_length

    pop rsi

    pop rdi

    pop rdx

    cmp rax, rdx

    jge .error

    xor rax, rax

    xor r8, r8

    .loop:

        mov r8b, [rdi + rax]

        mov [rsi + rax], r8b

        cmp r8b, 0

        je .end

        inc rax

        jmp .loop

    .error:

        xor rax, rax

    .end:

        xor r8, r8

        ret



read_line:

    xor rdx, rdx

    test rsi, rsi

    jz .error

    .loop:

        push rdx

	push rdi

	push rsi

        call read_char

	pop rsi

	pop rdi

        pop rdx

        cmp rdx, 0

        jne .process

        cmp al, 0x9

        je .loop

        cmp al, 0xA

        je .loop

        cmp al, 0x20

        je .loop

    .process:

        cmp rsi, rdx

        je .error

        cmp al, 0xA

        jne .save

        xor rax, rax

    .save:

        mov [rdi + rdx], al

        cmp rax, 0

        je .success

        inc rdx

        jmp .loop

    .error:

        xor rax, rax

        xor rdx, rdx

        ret

    .success: 

        mov rax, rdi

        ret